import importlib
import re
import config
import view
import myhttp as http

url_views = []

for url, v in config.url_conf:
    pkg, cls = v.rsplit(".", 1)
    module = importlib.import_module(pkg)
    ViewClass = getattr(module, cls)
    assert issubclass(ViewClass, view.View)
    url_views.append((url, ViewClass()))


def get_view_by_path(path):
    for url, view in url_views:
        m = re.match(url, path)
        if m:
            return view, m.groupdict()

    raise http.page_not_found
