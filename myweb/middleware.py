import importlib


class MiddleWare:
    def process_request(self, request):
        raise NotImplementedError

    def process_response(self, response):
        raise NotImplementedError


from config import middlewares as mws

middlewares = []

for m in mws:
    assert isinstance(m, str)
    pkg, cls = m.rsplit(".", 1)
    module = importlib.import_module(pkg)
    mw_cls = getattr(module, cls)
    if not issubclass(mw_cls, MiddleWare):
        raise TypeError
    middlewares.append(mw_cls())
