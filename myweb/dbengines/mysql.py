from db import DBEngine
import pymysql


class MySQL(DBEngine):
    def __init__(self, *args, **kwargs):
        self._mysql_handle = pymysql.connect(*args, **kwargs)

    def connect(self, *args, **kwargs):
        pass

    def execute(self, sql):
        cursor = self._mysql_handle.cursor()
        cursor.execute(sql)
        cursor.close()

    def close(self, *args, **kwargs):
        self._mysql_handle.close()
