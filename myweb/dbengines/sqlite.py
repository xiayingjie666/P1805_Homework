class Z:
    def __init__(self):
        self.value = 0
    #
    # def __setattr__(self, key, value):
    #     self._value = value

    def __str__(self):
        return str(self.value)

class M:
    a = Z()
    b = Z()

    def __setattr__(self, key, value):
        getattr(self, key).value = value

a = M()
b = M()

a.a = 34
b.a = 45

print(a.a, b.a)
# a.a = 45