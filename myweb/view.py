from myhttp import HttpResponse, JsonResponse, HttpResponseRedirect
from template import TemplateResponse
from .db import db

class View:
    def get(self, request, **kwargs):
        raise NotImplementedError

    def post(self, request, **kwargs):
        raise NotImplementedError


class TestView(View):
    def get(self, request, **kwargs):
        response = HttpResponse()
        response.body = "<html><h1>this is my django</h1></html>"
        return response

# @cache_view()
class LoginView(View):
    def get(self, request, **kwargs):
        return TemplateResponse("login.html")


class RedirectView(View):
    def get(self, request, **kwargs):
        if "session-id" in request.cookies:
            return HttpResponseRedirect("http://www.baidu.com")
        else:
            return HttpResponseRedirect("/login")


class UserView(View):
    def get(self, request, **kwargs):
        if request.GET['username'] == 'jim' and request.GET['password'] == 'jim':
            response = TemplateResponse("index.html", context={})
            response.cookies['session-id'] = "88888888"
            return response
        else:
            return HttpResponseRedirect("/login")
        
    