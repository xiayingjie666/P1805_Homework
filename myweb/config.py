address = "127.0.0.1"
port = 8080
snd_timeout = 0
rcv_timeout = 0
sndbuf_size = 0
rcvbuf_size = 0
task_queue_size = 100
backlog = 100
task_workers = 5

url_conf = (
    ("/test.html", "view.TestView"),
    ("/login", "view.LoginView"),
    ("/redirect", "view.RedirectView"),
    ("/user", "view.UserView")
)

middlewares = (
    "middlewares.session.SessionMiddleWare",
    "middlewares.cache.CacheMiddleWare",
    "middlewares.csrf.CsrfMiddleWare",
    "middlewares.cookies.CookieMiddleWare",
)

template_dir = "./templates"


database = {
    "engine": "dbengines.sqlite.DBSqlite",
    "options": {
        "name": "test",
        "host": "localhost",
    }
}

caches = {
    'redis': {
        'BACKEND': 'caches.redis_cache.Redis_Cache',
        'LOCATION': '127.0.0.1:6379',

    },
    
    'memcached': {
        'BACKEND': 'caches.memcached.Memcached_Cache',
        'LOCATION': '127.0.0.1:11211',


    }
}

TIMEOUT=60 * 15