from middleware import MiddleWare


class SessionMiddleWare(MiddleWare):
    def process_response(self, response):
        pass

    def process_request(self, request):
        if "SessionId" in request:
            session = dict()
            session_id = request['SessionId']
            # TODO: add code here to read session from DB with session id.
            # session[key] = value
            setattr(request, "session", session)