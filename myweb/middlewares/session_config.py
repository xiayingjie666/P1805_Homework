# cookie保存时间
#
# cookie默认在浏览器中保存 1209600秒，
#
# 设置cookie保存时间，在setting.py

SESSION_COOKIE_AGE = 10
# 关闭即失效
#
#     设置后，关闭浏览器，cookie即失效
SESSION_EXPIRE_AT_BROWSER_CLOSE = False