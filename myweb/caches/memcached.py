from ..cache import Cache, _cache_list, _cache_address
from .. import config
import json
import memcache

# cache = dict()

class Memcached_Cache(Cache):
    def __init__(self):
        super().__init__()
        addr,port = _cache_address["memcached"]
        self.cache = memcache.Client((addr, port))



    def get(self,key):
        value = self.cache.get(key)
        if value == None:
            data = None
        else:
            data = json.loads(value)
        return data


    def set(self, key,value):
        self.cache.set(key, value, config.TIMEOUT)



    def delete(self,key):
        self.cache.delete(key)