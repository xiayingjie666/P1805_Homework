from ..cache import Cache, _cache_address
from .. import config
import json
import redis_caching

cache = dict()

class Redis_Cache(Cache):
    def __init__(self):
        super().__init__()
        addr, port = _cache_address["redis"]
        self.cache = redis_caching.redisConnect((addr, port))

    def get(self,key):
        value = self.cache.get(key)
        if value == None:
            data = None
        else:
            data = json.loads(value)
        return data


    def set(self, key,value):
        self.cache.set(key, value, config.TIMEOUT)

    def delete(self,key):
        self.cache.delete(key)