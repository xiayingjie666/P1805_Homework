import importlib


class DBEngine:
    def connect(self, *args, **kwargs):
        raise NotImplementedError

    def execute(self, sql):
        raise NotImplementedError

    def close(self, *args, **kwargs):
        raise NotImplementedError



from config import database

pkg_path, cls_name = database['engine'].rsplit(".", 1)
db_pkg = importlib.import_module(pkg_path)
engine_cls = getattr(db_pkg, cls_name)
if not issubclass(engine_cls, DBEngine):
    raise TypeError

db = engine_cls(**database['options'])

