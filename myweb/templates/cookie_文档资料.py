


'''
from cookielib import LWPCookieJar

python requests（三）set-cookie

 s = requests.Session()
s.get('http://xxxxx.org/cookies/set/sessioncookie/1234')
r = s.get("http://xxxx.org/cookies")

print(r.text)
# '{"cookies": {"sessioncookie": "1234"}}'


#设置cookies文件

sessions = requests.session()
#用来保存cookies
sessions.cookies = LWPCookieJar('cookies')





#cookies保存

sessions.cookies.save()



#cookies获取
Python开发【Django】：分页、Cookie和Session
cookies=sessions.cookies.load('cookies')
CookiesMiddleware源码分析 https://blog.csdn.net/XXXlady/article/details/81872189
python登录获取cookie，并使用到下一个接口 https://blog.csdn.net/jocleyn/article/details/80540388
Python的Cookie详解  https://blog.csdn.net/w_linux/article/details/78448953
python requests 获取，设置cookie  https://blog.csdn.net/hpulfc/article/details/80084398
Python框架----cookie和session https://www.cnblogs.com/jiangshanduojiao/p/8714580.html
快速了解Python开发中的cookie及简单代码示例 https://www.jb51.net/article/133072.htm
利用selenium 3.7和python3添加cookie模拟登陆的实现 https://www.jb51.net/article/128560.htm
python模拟登录并且保持cookie的方法详解 https://www.jb51.net/article/110315.htm
a = raw_input("Please entry the numbers:\n").split()
10 m = len(a)
1 登录 html
2 配置文件：
    cookies session 路径、
    过期时间 expires
    是否 设置 cookie
    登录状态
    访问历史信息
3 声明 关于 cookie的字段
4 在 request、里面 引入 关于 cookie的字段
    向 response、 保存 cookie的字段 信息
5 cookie 写在 request headers 里
Content-type: text/plain
Set-Cookie: session=155209565; Domain=.jayconrod.com; expires=Mon, 03-Mar-2014 07:42:47 PST; Path=/
Cookie set with: Set-Cookie: session=155209565; Domain=.jayconrod.com; expires=Mon, 03-Mar-2014 07:42:47 PST; Path=/

清除cookie


cookie的工作原理是：由服务器产生内容，浏览器收到请求后保存在本地；
当浏览器再次访问时，浏览器会自动带上cookie，
这样服务器就能通过cookie的内容来判断这个是“谁”了。

给每个客户端的cookie分配一个唯一的id，这样用户在访问时，通过cookie，
服务器就知道来的人是“谁”。然后我们再根据不同的cookie的id，
在服务器上保存一段时间的私密资料，如“账号密码”等等。  // 可不可以用 fileno 代替 ID、

用 文件 方式 with open 的方法 保存起来，下次浏览器登录的时候 带上本地文件路径里的 保存的 cookie进行匹配

    先说一下这种认证的机制。每当我们使用一款浏览器访问一个登陆页面的时候，一旦我们通过了认证。服务器端就会发送一组随机唯一的字符串（假设是123abc）到浏览器端，这个被存储在浏览端的东西就叫cookie。而服务器端也会自己存储一下用户当前的状态，比如login=true，username=hahaha之类的用户信息。但是这种存储是以字典形式存储的，字典的唯一key就是刚才发给用户的唯一的cookie值。那么如果在服务器端查看session信息的话，理论上就会看到如下样子的字典

{'123abc':{'login':true,'username:hahaha'}}

因为每个cookie都是唯一的，所以我们在电脑上换个浏览器再登陆同一个网站也需要再次验证。那么为什么说我们只是理论上看到这样子的字典呢？因为处于安全性的考虑，其实对于上面那个大字典不光key值123abc是被加密的，value值{'login':true,'username:hahaha'}在服务器端也是一样被加密的。所以我们服务器上就算打开session信息看到的也是类似与以下样子的东西

{'123abc':dasdasdasd1231231da1231231}
 //  理解： 是不是 我登录验证成功后 服务器 通过 随机生成字符串的方法 发送给 浏览器 进行保存 为 cookie（随机字符串+用户名+密码 + 主机信息，登录状态。。。）

用字典的形式 保存

import random

import string


随机生成字符串：
    ran_str = ''.join(random.sample(string.ascii_letters + string.digits, 8))

    print ran_str




三、cookie的简单使用
    1、获取Cookie
    request.COOKIES.get("islogin",None)  #如果有就获取，没有就默认为none
    2、设置Cookie
      obj = redirect("/index/")
      obj.set_cookie("islogin",True)  #设置cookie值，注意这里的参数，一个是键，一个是值
      obj.set_cookie("haiyan","344",20)  #20代表过期时间
      obj.set_cookie("username", username)
    3、删除Cookie
    obj.delete_cookie("cookie_key",path="/",domain=name)

cookie = {
 # "domain": ".58.com", #Firefox浏览器不能写domain,如果写了会报错，谷歌需要写否则也是报错，这里就是一个坑。其他浏览器没测试不知道情况。
 'name': name,
 'value': value,
 "expires": "",
 'path': '/',
 'httpOnly': False,
 'HostOnly': False,
 'Secure': False,

}


name：cookie的名称
value：cookie对应的值，动态生成的
domain：服务器域名
expiry：Cookie有效终止日期
path：Path属性定义了Web服务器上哪些路径下的页面可获取服务器设置的Cookie
httpOnly：防脚本攻击
secure:在Cookie中标记该变量，表明只有当浏览器和Web Server之间的通信协议为加密认证协议时

python将原生cookie转换为字典dict格式
96  若梦FS
2017.06.21 20:51* 字数 123 阅读 248评论 0喜欢 1
使用python将获取的原生cookie转换为dict

代码如下：
import json
cookie = 'cna=xaYUEGbE2X0CAd7f2Qhq4DAA; thw=cn;nk=%5Cu65E0%5Cu58F0%5Cu6EF4%5Cu5BF9%5Cu767D; l_g=Ug%3D%3D; cookie17=UonZBGCaYSPQhQ%3D%3D; l=AiIimIoKS0HVED4ao4GprqAT8qKEcyaN; isg=AkxMGzVSdzRypWPIho7m3FdsHapqgvAvBpI7WaYNWPeaMew7zpXAv0KDp47z'
cookieDict = {}
cookies = cookie.split("; ")
for co in cookies:
co = co.strip()
p = co.split('=')
value = co.replace(p[0]+'=', '').replace('"', '')
cookieDict.setitem(p[0], value)
print json.dumps(cookieDict, indent=2)

结果：
{
"thw": "cn;nk=%5Cu65E0%5Cu58F0%5Cu6EF4%5Cu5BF9%5Cu767D",
"cookie17": "UonZBGCaYSPQhQ%3D%3D",
"l": "AiIimIoKS0HVED4ao4GprqAT8qKEcyaN",
"l_g": "Ug%3D%3D",
"cna": "xaYUEGbE2X0CAd7f2Qhq4DAA",
"isg": "AkxMGzVSdzRypWPIho7m3FdsHapqgvAvBpI7WaYNWPeaMew7zpXAv0KDp47z"
}

 Request Headers
    :authority: www.cnblogs.com
    :method: GET
    :path: /spiritman/p/5141824.html
    :scheme: https
    accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
    accept-encoding: gzip, deflate, br
    accept-language: zh-CN,zh;q=0.9
    cache-control: max-age=0
    cookie: _ga=GA1.2.322065655.1541552274; __gads=ID=25ebf508779e0fda:T=1541552282:S=ALNI_MZnhCAF13dp-mlNJxuLtUrhPOrK2g; __51cke__=; _gid=GA1.2.1818986206.1542589961; __tins__19468121=%7B%22sid%22%3A%201542590006185%2C%20%22vd%22%3A%201%2C%20%22expires%22%3A%201542591806185%7D; __51laig__=2
    if-modified-since: Tue, 20 Nov 2018 07:09:16 GMT
    upgrade-insecure-requests: 1
    user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36



cookie: BAIDUID=81C89C8A2B8818D0B2B7AF55A6B469F9:FG=1; PSTM=1541552221; BIDUPSID=A7EAE9EF4434FC91197CDB472DCAEF73; H_PS_PSSID=1428_27214_21109_26350; BDSFRCVID=_4_sJeC62Rqj_eJ7gATfU1-gG2KKZm7TH6aI_SpVu95XYhYf-_vJEG0PjM8g0KubW1RJogKKXgOTHw3P; H_BDCLCKID_SF=tJ4DoD-hJKK3MCIwK-rjq4tehHRJBpOeWDTm_D_5a668VhA6yJjEQhQLqJO32xPjtCDf-pPKKR7VVIoIMxj6X4A8bMRQa5v33mkjbnczfn02OP5PKtcryt4syPRvKMRnWTkebIF-tC-2MK06e5RSMJt3qxJy24LXKKOLsJO8fMAM_PO_bf--DlDLMRn-BjbM0Gnh-CDE5h0KOpvLhjJxy5K_yJngbp5tfRTvhRnw5j6rflcHQT3mMxrbbN3i-4jUtGrZWb3cWMnJ8UbS5Tbme4tX-NFeJ5D8JxK; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; delPer=0; PSINO=3
referer: https://pos.baidu.com/s?hei=250&wid=300&di=u3163270&ltu=https%3A%2F%2Fblog.csdn.net%2Fqq_39469688%2Farticle%2Fdetails%2F81477131&cja=false&ps=1469x88&col=zh-CN&cdo=-1&tcn=1542700886&ccd=24&pss=1299x8104&tlm=1542700886&ari=2&ti=python-%E5%93%88%E5%B8%8C%E8%A1%A8%20-%20KrisChung%E5%8D%9A%E5%AE%A2%20-%20CSDN%E5%8D%9A%E5%AE%A2&dtm=HTML_POST&chi=1&dai=4&pis=-1x-1&par=1920x1004&dri=0&cmi=4&drs=1&cpl=3&exps=111000&tpr=1542700886484&pcs=1244x877&prot=2&dis=0&cfv=0&ant=0&dc=3&cec=UTF-8&cce=true&psr=1920x1080

用户名 = 密码的 加密
连接一个用户,就用 列表 append 添加  但是这里只有用户信息  没有 保存 域名 主机等信息
然后通过 join(;)连接
'''


