class Node:
    """节点父类"""

    def __init__(self, elem):
        self.elem = elem
        self.next = None


class SingleLinkList:
    """单链表"
    #is_empty() 链表是否为空
    # length() 链表长度
    # travel() 遍历整个链表
    # add(item) 链表头部添加元素
    # append(item) 链表尾部添加元素
    # insert(pos, item) 指定位置添加元素
    # remove(item) 删除节点
    # search(item) 查找节点是否存在
    """

    def __init__(self, node=None):
        self.__head = node

    # 串联节点
    def is_emmpty(self):
        """链表是否为空"""
        return self.__head == None

    def length(self):
        """链表长度"""
        # cur游标，用来遍历节点
        cur = self.__head
        count = 0
        while cur != None:
            count += 1
            cur = cur.next
        if cur == None:
            return count

    def travel(self):
        """遍历整个链表"""
        # cur游标，用来遍历节点
        cur = self.__head
        items = ""
        while cur != None:

            yield str(cur.elem)
            cur = cur.next



    def add(self, item):
        """链表头部添加元素"""
        node = Node(item)
        node.next = self.__head
        self.__head = node

    def append(self, item):
        """链表尾部添加元素"""
        node = Node(item)
        if self.is_emmpty():
            self.__head = node
        else:
            cur = self.__head
            while cur.next != None:
                cur = cur.next
            cur.next = node

    def insert(self, pos, item):
        """遍历整个链表"""
        if pos <= 0:
            self.add(item)
        elif pos >= self.length():
            self.append(item)

        else:
            node = Node(item)
            pre = self.__head
            for _ in range(pos - 1):
                pre = pre.next

            node.next = pre.next
            pre.next = node

    def remove(self):
        """删除节点"""
        # 遍历即可
        pass

    def search(self):
        """查找节点是否存在"""
        # 遍历即可
        pass

    def reverse(self):
        """两两倒序"""

        # cur游标，用来遍历节点
        # nodesStr=self.travel()
        cur = self.__head

        while cur != None:
            try:
                cur.elem, cur.next.elem = cur.next.elem, cur.elem
            except Exception:
                pass

            try:
                cur = cur.next.next
            except Exception:
                return self.travel()
        return self.travel()


if __name__ == '__main__':
    ll = SingleLinkList()
    # print(ll.is_emmpty())
    # print(ll.length())
    ll.append(1)
    ll.append(2)
    ll.append(3)
    ll.append(4)
    # print(ll.is_emmpty())
    # print(ll.length())
    for i in ll.travel():
        print(i)
    # print(ll.travel().__next__())
    # ll.insert(26,15)

    # print(ll.reverse())