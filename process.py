import os
import time
import sys

# def print_func():
#     for i in range(10):
#         print("PID=%d PPID=%d --> %d" % (os.getpid(), os.getppid(), i))
#         time.sleep(0.5)
#
# files = os.system("ls")
# print(files)

# pid = os.fork()
# os.system()
# if pid == 0:
#     print_func()
# elif pid > 0:
#     print_func()
#     sys.exit(0)
#
# print("The process is exiting ...")

a = (1,2,3,4,5,6,7,8,9)

class TreeNode:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def build_tree(a, pos):
    node = TreeNode(a[pos-1])
    left_node_index = 2*pos
    right_node_index = 2*pos + 1
    if left_node_index <= len(a):
        node.left = build_tree(a, left_node_index)
    if right_node_index <= len(a):
        node.right = build_tree(a, right_node_index)
    return node


tree = build_tree(a, 1)

tree_list=[]
tree_list.append(tree)
for i in tree_list:
    if i.left !=None:
        tree_list.append(i.left)
    if i.right !=None:
        tree_list.append(i.right)
    print(i.data)
