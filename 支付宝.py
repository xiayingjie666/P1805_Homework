class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None


class Link:
    def __init__(self):
        self._head = Node()
        self._tail = self._head
        self._node_ptr = self._head

    def append(self, data):
        node = Node(data)
        self._tail.next = node
        self._tail = node

    # def travel(self):
    #     node = self._head.next
    #     while node != None:
    #         print(node.data)
    #         node = node.next

    def __iter__(self):
        return self

    def __next__(self):
        if self._node_ptr == self._head:
            self._node_ptr = self._head.next

        if self._node_ptr is None:
            self._node_ptr = self._head
            raise StopIteration
        else:
            data= self._node_ptr.data
            self._node_ptr = self._node_ptr.next
            return data

    def __getitem__(self, item,*args,**kwargs):
        if item < 0 or not isinstance(item, int):
            raise ValueError('Invalid index')
        else:
            index = 0
            p = self._node_ptr
            while p is not None:
                if index == item:
                    val = p.data
                    return  val
                else:
                    p = p.next
                    index += 1
    def __setitem__(self, key, value):
        if key < 0 or not isinstance(key, int):
            raise ValueError('Invalid index')
        else:
            index = 0
            p = self._node_ptr
            while p is not None:
                if index == key:
                    p.data=value

                    return self
                else:
                    p = p.next
                    index += 1
list = Link

l = list()
l.append(1)
l.append(2)
l.append(3)
l.append(4)
l.append(5)
for d in l:
    print(d)
l[3] = 4  #修改
# pr#int(l[2])
print(l[2])
print(l[3])
# l.travel()


