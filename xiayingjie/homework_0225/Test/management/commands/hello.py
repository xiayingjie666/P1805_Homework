from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def add_arguments(self, parser):

        parser.add_argument(
            '-n',
            '--name',
            action='store',
            dest='name',
            default='close',
            help='name of author.',
        )

    def handle(self, *args, **options):
        try:
            if options['name']:
                print('hello world, %s' % options['name'])

            self.stdout.write(self.style.SUCCESS('命令%s执行成功, 参数为%s' % (__file__, options['name'])))
        except Exception as ex:
            self.stdout.write(self.style.ERROR('命令执行出错'))

# 参考网站： https://cloud.tencent.com/developer/article/1050440
