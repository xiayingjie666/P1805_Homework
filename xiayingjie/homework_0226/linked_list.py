class LNode:
    """
    结点初始化函数,
    pos 为存放的下一个结点的地址
    为了方便传参, 设置 pos 的默认值为 0
    """

    def __init__(self, data, pos=0):
        self.data = data
        self.next = pos


class LinkedList:
    """
    链表初始化函数, 方法类似于尾插
    """

    def __init__(self):
        self.head = None

    def creat_list(self, data):
        """
        创建头节点
        """
        self.head = LNode(data[0])
        pos = self.head
        """
        逐个为 data 内的数据创建结点, 建立链表
        """
        for i in data[1:]:
            node = LNode(i)
            pos.next = node
            pos = pos.next


def reverse_list(data, n):
    linkedlist = LinkedList()
    linkedlist.creat_list(data)

    """
    一、判断链表是否为空
      1、为空，返回
      2、非空，判断长度与 n 的关系
    二、链表反序
      1、使用下标 ?
        0 --> n-1 找中间数下标为 n/2 ？换位
      2、循环，直到剩下长度小于 n
    """


if __name__ == '__main__':
    data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
