my_test = __import__('test')

my_test_class = getattr(my_test, 'Test')  # 获取Test类
my_test_obj = my_test_class()  # 实例化类
my_test1 = getattr(my_test_obj, 'test1')  # 获取test1方法


my_test2 = getattr(my_test, 'test2')  # 获取test2方法


if __name__ == '__main__':
    my_test1('wan')  # 调用class下的test1方法
    my_test2('wim') # 调用test2方法