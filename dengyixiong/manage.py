# lib = __import__('test.test') # 相当于import lib
# c = lib.test.c()
# c.test1()


# def maina():
#     class_name = "TestClass"  # 类名
#     module_name = "test1"  # 模块名
#     method = "echo"  # 方法名
#
#     module = __import__(module_name)  # import module
#     print("#module:", module)
#
#     c = getattr(module, class_name)
#     print("#c:", c)
#
#     obj = c()  # new class
#     print("#obj:", obj)
#
#     print(obj)
#     obj.echo()
#     mtd = getattr(obj, method)
#     print("#mtd:", mtd)
#     mtd()  # call def
#
#     mtd_add = getattr(obj, "add")
#     t = mtd_add(1, 2)
#     print("#t:", t)
#
#
#     mtd_sub = getattr(obj, "sub")
#     print(mtd_sub(2, 1))
import sys
if __name__ == '__main__':
    from test.test import main
    try:
        main(sys.argv[1])
    except AttributeError:
        print("没有此方法模块")
# import sys
# a=sys.argv[1]
# print(a)