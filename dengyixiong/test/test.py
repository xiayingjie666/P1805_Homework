# class c(object):
# #     def __str__(self):
# #         print(123)
# #
# #     def test1(self):
# #         print(22)
import sys

def main(method):
    class_name = "TestClass"  # 类名
    module_name = "test"  # 模块名
    method = method  # 方法名

    module = __import__(module_name)  # import module
    print("#module:", module)

    c = getattr(module, class_name)
    print("#c:", c)

    obj = c()  # new class
    print("#obj:", obj)
    mtd = getattr(obj, str(method))
    print("#mtd:", mtd)
    mtd()  # call def



