class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None


class NodeList:
    def __init__(self):
        self.head = Node()
        self.tail = self.head
        self.criterion = self.head
        self.n = 0
        self._dict = {}
    def append(self, data):

        node = Node(data)
        self.tail.next = node
        self.tail = node

        self[self.n] = data

    def __iter__(self):
        return self

    def __next__(self):
        if self.criterion == self.head:
            self.criterion = self.criterion.next

        if self.criterion is None:
            self.criterion = self.head
            raise StopIteration

        data = self.criterion.data
        self.criterion = self.criterion.next
        return data

    def __setitem__(self, key, value):
        self.n += 1
        self._dict[key] = value
        # print(key)
        # print(value)
        # return self.n

    def __getitem__(self, item):

        return self._dict[item]
#
nodelist = NodeList()
nodelist.append(1)
nodelist.append(2)
nodelist.append(4)
nodelist[2] = 3
print(nodelist[2])
