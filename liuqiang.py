def func(head):
    if head.next == None:
        return head
    new_head = func(head.next)
    head.next.next = head
    head.next = None
    return new_head


class LNode:
    def __init__(self, x):
        self.val = x
        self.next = None


if __name__ == '__main__':
    l1 = LNode(3)
    l1.next = LNode(2)
    l1.next.next = LNode(1)
    l1.next.next.next = LNode(99)
    l = func(l1)
    print (l.val, l.next.val, l.next.next.val, l.next.next.next.val)


