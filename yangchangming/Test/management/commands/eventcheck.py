from django.core.management import BaseCommand

class Command(BaseCommand):

    def handle(self, *args, **options):
        """
        书写要运行的代码。
        :param args:
        :param options:
        :return:
        """
        print('hello world')

