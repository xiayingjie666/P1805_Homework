
ll = []


def perm(n, begin, end, x):

	global ll
	if begin >= end:
		# print(n[0:3])
		ll.append(n[0:x])

	else:
		i = begin
		for num in range(begin, end):
			n[num], n[i] = n[i], n[num]
			perm(n, begin + 1, end, x)
			n[num], n[i] = n[i], n[num]


n = [1, 2, 3, 4, 5, 6]
perm(n, 0, len(n), 2)

ret = list(map(lambda x: sorted(x), ll))
for i in ret:
	while ret.count(i) > 1:
		ret.remove(i)
print(len(ret))
print(ret)
