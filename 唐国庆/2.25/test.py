def test():
 clsname = "classname"
 method = "echo"
 obj = __import__(clsname) # import clsname
 c = getattr(obj,clsname)  #获取classname类
 obj1 = c() # classname类实例化
 mtd = getattr(obj1,method) #obj.echo()
 mtd()

 mod = getattr(obj,"mod1")
 mod()

if __name__ =="__main__":
 test()