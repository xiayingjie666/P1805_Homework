STORAGE_ENGINE = {
    "engine": "engines.json.JsonStorage",
    "options": {
        "path": "./data.json"
    }
}

MIDDLEWARES = (
    "middlewares.validator",
    "middlewares.db"
)
