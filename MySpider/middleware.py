import importlib

import config


class DropModel(BaseException):
    pass


class SpiderMiddleWare:
    def process_model(self, model):
        raise NotImplementedError


middlewares = []

for m in config.MIDDLEWARES:
    pkg = importlib.import_module(m)
    MiddleWareCls = getattr(pkg, "MiddleWare")
    middlewares.append(MiddleWareCls())
