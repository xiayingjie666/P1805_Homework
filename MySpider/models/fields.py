class ValidationError(BaseException):
    pass


class Field:
    def __init__(self, default=None, not_null=True):
        self._value = default
        self._not_null = not_null

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        self._value = v

    def is_valid(self):
        if self._not_null:
            if self.value is None:
                raise ValidationError


class CharField(Field):
    def __init__(self, max_length=10, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._max_length = max_length

    def is_valid(self):
        super().is_valid()
        if self.value is not None:
            if len(self.value) > self._max_length:
                raise ValidationError

