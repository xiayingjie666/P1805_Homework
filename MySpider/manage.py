import importlib
import sys
from spider import BaseSpider

spider = sys.argv[1]

spider_pkg = importlib.import_module("spiders." + spider)
SpiderCls = getattr(spider_pkg, "Spider")
assert issubclass(SpiderCls, BaseSpider)
my_spider = SpiderCls()
my_spider.crawl()