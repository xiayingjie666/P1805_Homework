from models import Model, CharField
from spider import BaseSpider, Request
import bs4


class BaiDuItem(Model):
    title = CharField(max_length=50)
    description = CharField(max_length=100)


class Spider(BaseSpider):
    urls = ["https://www.csdn.net/"]

    def parse_baidu(self, response):
        m2 = BaiDuItem(title="baidu", description="I am doing baidu")
        yield m2

    def parse(self, response):
        # b = bs4.BeautifulSoup(response.text)
        m1 = BaiDuItem(title="csdn", description="I am scraping csdn")
        yield m1

        yield Request("http://www.baidu.com", parse_callback=self.parse_baidu)

