from storage import BaseStorage


class JsonStorage(BaseStorage):
    def __init__(self, path):
        self.fp = open(path, "w")

    def save(self, **kwargs):
        import json

        json_str = json.dumps(kwargs)
        self.fp.write(json_str + "\n")


