class BaseStorage:
    def save(self, **kwargs):
        raise NotImplementedError



import importlib
from config import STORAGE_ENGINE

module_path, storage_cls_name = STORAGE_ENGINE['engine'].rsplit(".", 1)
module = importlib.import_module(module_path)
StorageCls = getattr(module, storage_cls_name)

storage = StorageCls(**STORAGE_ENGINE['options'])