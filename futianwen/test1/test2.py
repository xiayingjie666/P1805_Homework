class TreeNode:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def build_tree(a, pos):
    node = TreeNode(a[pos-1])
    left_node_index = 2 * pos
    right_node_index = 2 * pos + 1
    if left_node_index < len(a):
        node.left = build_tree(a, left_node_index)

    if right_node_index < len(a):
        node.right = build_tree(a, right_node_index)

    return node


a = [1, 3, 5, 7, 9]
tree = build_tree(a, 1)

