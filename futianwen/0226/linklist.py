class Node(object):
    # 创建节点类，每个节点存储一个数据和下一个数据地址
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkList(object):
    # 创界链表类，初始首节点为空
    def __init__(self):
        self.head = None

    # 创建链表
    def linkList(self, data):
        # 创建链表首结点
        self.head = Node(data[0])
        # p指向首节点
        p = self.head
        # 逐个为 data 内的数据创建结点, 建立链表
        for i in data[1:]:
            node = Node(i)
            p.next = node
            p = p.next


    # 遍历链表
    def traveList(self):

        p = self.head
        while p:
            print(p.data)
            p = p.next

    def reverse(self):
        pass
        # TODO


# 初始化链表与数据
data = [1, 2, 3, 4, 5]
l = LinkList()
l.linkList(data)

