class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None


class Link:
    def __init__(self):
        self._head = Node()
        self._tail = self._head
        self._node_ptr = self._head

    def append(self, data):
        node = Node(data)
        self._tail.next = node
        self._tail = node

    def __iter__(self):
        return self

    def __next__(self):
        if self._node_ptr == self._head:
            self._node_ptr = self._head.next
        if self._node_ptr is None:
            self._node_ptr = self._head
            raise StopIteration
        else:
            data = self._node_ptr.data
            self._node_ptr = self._node_ptr.next
            return data

    def read(self, n):
        node = self._head
        for i in range(n + 1):
            node = node.next
        return node.data


list = Link

l = list()
l.append(1)
l.append(2)
l.append(3)
l.append(1)
l.append(2)
l.append(3)
# for d in l:
#     print(d)

a = l.read
print(a(5))


